import React, { Component } from 'react';
import './GameSquearComponent.css';

class GameSquearComponent extends Component {

  itemClickHandler = (e) => {
    let classes = e.target.classList;
    if (classes.contains('boxWithRing')) {
      classes.add('hide');
      console.log(e.target)
    } else if (classes.contains('hide')) {
      return;
    } else if (classes.contains('box')) {
      classes.add('hide')
    }
    this.props.action();
  }
  
  render () {
    return <div className="boxesConteiner">
        {this.props.states.map((item, index) => (
          <div
            key={index}
            onClick={this.itemClickHandler}
            className={item.isClass.join(" ")}
          />
        ))}
      </div>;
  }
}


export default GameSquearComponent;
