import React from 'react';
import './ButtonReset.css';

const Button = ({action, children}) =>
    <button className="button" onClick={action}>{children}</button>


export default Button;
