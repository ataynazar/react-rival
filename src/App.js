import React, { Component } from 'react';
import './App.css';
import Button from './components/ButtonComponent/ButtonReset';
import GameSquearComponent from './components/GameSquearComponent/GameSquearComponent.js';


class App extends Component {
  state = {
    boxes: [],
    count: 0
  };

  itemsArray = () => {

    let randomNumber = Math.floor(Math.random() * (36 - 0) + 0);
    const boxes = [];
    const box = {heveARing: false, isClass : ['box']};
    for (let i = 0; i < 36 ; i++) {
      if (i === randomNumber) {
        boxes.push({ heveARing: false, isClass: ["box", "boxWithRing"] });
      } else {
        boxes.push(box);
      }
    }
    this.setState({boxes});

  };

  updateCount = () => {

    this.setState({
      count: this.state.count + 1
    })
    
  }

  componentDidMount(){
    this.itemsArray();
  }
  
  render() {
    return (
      <div className="App">
      <GameSquearComponent action={this.updateCount} states={this.state.boxes}/>
      <h2 className="counts">Tries: {this.state.count}</h2>
      <Button>Reset</Button>
      </div>
    );
  }
}

export default App;
